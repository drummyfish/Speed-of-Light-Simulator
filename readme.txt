  ====================================================================
  |                                                                  |
  |                   SPEED OF LIGHT SIMULATOR                       |
  |                                                                  |
  |  v 1.0, Miloslav Èíž, 2014                                       |
  |                                                                  |
  ====================================================================

  This is a simple JavaScript demonstration of distortions that happen
due to finite speed of light.
  To run the script, simply open it with your internet browser. The
script was only tested with Google Chrome.

This is released into the public domain under CC0 1.0.